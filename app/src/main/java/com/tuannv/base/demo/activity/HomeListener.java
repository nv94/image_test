package com.tuannv.base.demo.activity;

import com.tuannv.base.demo.base.recycleview.BaseViewAdapter;
import com.tuannv.base.demo.data.model.ImageItem;

public interface HomeListener extends BaseViewAdapter.Listener {
    void showDetailInfo(ImageItem dataInfo);
}