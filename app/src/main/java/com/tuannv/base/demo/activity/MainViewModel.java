package com.tuannv.base.demo.activity;

import android.app.Application;
import android.content.Context;
import android.media.Image;
import android.util.Log;

import com.tuannv.base.demo.base.BaseViewModel;
import com.tuannv.base.demo.data.local.dao.LocalRepository;
import com.tuannv.base.demo.data.model.Example2;
import com.tuannv.base.demo.data.model.ImageItem;
import com.tuannv.base.demo.data.remote.AppApiHelperImpl;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends BaseViewModel {
    private AppApiHelperImpl appApiHelper;
    public MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private LocalRepository repository;
    public MutableLiveData<List<ImageItem>> listImage = new MutableLiveData<>();

    public MainViewModel(@NonNull Application application, Context context) {
        super(application);
        appApiHelper = new AppApiHelperImpl();
        repository = new LocalRepository(context);
    }

    public void fetchUserInfo(int page) {
        getCompositeDisposable().add(appApiHelper.getListData(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe(disposable -> {
                loading.postValue(true);
            })
            .doAfterTerminate(() -> {
                loading.postValue(false);
            })
            .subscribe(data -> {
                List<ImageItem> list = new ArrayList<>();
                for (ImageItem datum : data) {
                    list.add(new ImageItem(datum.getUrl()));
                }
                listImage.postValue(list);
            }, throwable -> {
                Log.e("tuanbg", throwable.getMessage());
            })
        );
    }

    public void getData(Example2 example) {
        getCompositeDisposable().add(repository.insertExample(example)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(data -> {
                Log.e("tuanbg", data + "");
                getExample();
            }, throwable -> {
                Log.e("throwable", throwable + "");
            }));
    }

    public void getExample() {
        getCompositeDisposable().add(repository.getExample2()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(data -> {
                Log.e("tuanbg", data + "");
            }, throwable -> {
                Log.e("throwable", throwable + "");
            }));
    }
}
