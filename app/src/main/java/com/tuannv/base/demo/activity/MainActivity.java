package com.tuannv.base.demo.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.tuannv.base.demo.BaseApplication;
import com.tuannv.base.demo.base.recycleview.SingleTypeAdapter;
import com.tuannv.base.demo.data.model.ImageItem;
import com.tuannv.base.demo.fragment.imagedialog.ImageDialogFragment;
import com.tuannv.base.truyenhh.R;
import com.tuannv.base.demo.base.BaseActivity;
import com.tuannv.base.truyenhh.databinding.ActivityMainBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements
        SwipeRefreshLayout.OnRefreshListener,HomeListener {
    private MainViewModel viewModel;
    private SingleTypeAdapter<ImageItem> adapter;
    private GridLayoutManager manager;
    private List<ImageItem> listAllImage = new ArrayList<>();
    private int page = 1 ;
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        viewModel = new MainViewModel(BaseApplication.getInstance(), this);
        initData();
        viewModel.fetchUserInfo(page);
    }

    private void initView() {
        setSupportActionBar(getViewDataBinding().toolbar);
        getViewDataBinding().refeshlayout.setProgressViewOffset(false, 50,50);
        adapter =
                new SingleTypeAdapter<>(this, R.layout.item_image);
        manager = new GridLayoutManager(this,2);
        getViewDataBinding().recyclerview
                .setLayoutManager(manager);
        getViewDataBinding().recyclerview.setAdapter(adapter);
        adapter.setPresenter(this);
    }

    private void initData(){
        viewModel.listImage.observe(this, data -> {
            if (data == null || data.size() == 0) return;
            listAllImage.addAll(data);
            adapter.set(listAllImage);
        });
        viewModel.loading.observe(this, isLoading -> {
            getViewDataBinding().refeshlayout.setRefreshing(isLoading);
        });
        getViewDataBinding().recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int scrolledItems = manager.findLastVisibleItemPosition();
                int totalItems = manager.getItemCount();
                if (totalItems <= (scrolledItems + 2)) {
                    viewModel.fetchUserInfo(page ++);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);
        SearchView searchView =
            (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.app_bar_search));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.app_bar_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        viewModel.fetchUserInfo(1);
    }

    @Override
    public void showDetailInfo(ImageItem dataInfo) {
        ImageDialogFragment dialog = ImageDialogFragment.newInstance(dataInfo.getUrl());
        dialog.show(getSupportFragmentManager(), "MyCustomDialog");
    }
}
