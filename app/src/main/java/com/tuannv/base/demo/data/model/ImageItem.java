package com.tuannv.base.demo.data.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageItem {

@SerializedName("breeds")
@Expose
private List<Object> breeds = new ArrayList<>();
@SerializedName("height")
@Expose
private int height;
@SerializedName("id")
@Expose
private String id;
@SerializedName("url")
@Expose
private String url;
@SerializedName("width")
@Expose
private int width;


    public ImageItem(String url) {
        this.url = url;
    }

    public List<Object> getBreeds() {
return breeds;
}

public void setBreeds(List<Object> breeds) {
this.breeds = breeds;
}

public int getHeight() {
return height;
}

public void setHeight(int height) {
this.height = height;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getUrl() {
return url;
}

public void setUrl(String url) {
this.url = url;
}

public int getWidth() {
return width;
}

public void setWidth(int width) {
this.width = width;
}

}