package com.tuannv.base.demo.data.remote;

import com.tuannv.base.demo.data.model.Example2;
import com.tuannv.base.demo.data.model.ImageItem;

import io.reactivex.Single;

/**
 * Create by Tuannv
 * Define function call to sever using RX
 * ex: Single<BlogResponse> getBlogApiCall();
 **/
public interface AppApiHelper {
    Single<ImageItem[]> getListData(int page);
}
