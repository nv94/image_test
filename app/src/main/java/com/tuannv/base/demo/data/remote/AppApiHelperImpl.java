package com.tuannv.base.demo.data.remote;

import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.tuannv.base.demo.data.model.Example2;
import com.tuannv.base.demo.data.model.ImageItem;

import io.reactivex.Single;

/**
 * Class AppApiHelperImpl implement AppApiHelper
 * ex: @Override
 * public Single<BlogResponse> getBlogApiCall() {
 * return Rx2AndroidNetworking.get(EndPoint.ENDPOINT)
 * //               .addHeaders(mApiHeader.getProtectedApiHeader()) Add Header when request
 * .build()
 * .getObjectSingle(BlogResponse.class);
 * }
 **/
public class AppApiHelperImpl implements AppApiHelper {
    @Override
    public Single<ImageItem[]> getListData(int page) {
        return Rx2AndroidNetworking.get(AppEndPoint.BASE_URL)
            .addQueryParameter("page",String.valueOf(page))
            .addQueryParameter("limit", "20")
            .addHeaders("x-api-key", "d7caee0a-ad59-4e59-9a35-cc10cb5e47e2")
            .build()
            .getObjectSingle(ImageItem[].class);
    }
}
