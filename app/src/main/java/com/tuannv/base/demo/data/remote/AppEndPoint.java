package com.tuannv.base.demo.data.remote;

/**
 * Class define Endpoint app
 **/
public class AppEndPoint {
    public static final String BASE_URL = "https://api.thecatapi.com/v1/images/search";
}
