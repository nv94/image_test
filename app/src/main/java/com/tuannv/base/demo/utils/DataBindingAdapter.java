package com.tuannv.base.demo.utils;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.tuannv.base.demo.data.model.ImageItem;

public class DataBindingAdapter {
    @BindingAdapter("image")
    public static void setImage(ImageView view, ImageItem item) {
        Glide.with(view.getContext()).load(item.getUrl()).into(view);
    }
}
