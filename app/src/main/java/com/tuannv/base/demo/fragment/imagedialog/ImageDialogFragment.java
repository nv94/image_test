package com.tuannv.base.demo.fragment.imagedialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.tuannv.base.truyenhh.R;

public class ImageDialogFragment extends DialogFragment {

    public static ImageDialogFragment newInstance(String url) {

        Bundle args = new Bundle();
        args.putString("url",url);
        ImageDialogFragment fragment = new ImageDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String getUrl(){
        Bundle bundle = getArguments();
        return bundle == null ? "" : bundle.getString("url");
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imageView = view.findViewById(R.id.image_detail);
        Glide
                .with(this)
                .load(getUrl())
                .into(imageView);
    }
}
